import { useState, useEffect } from 'react'
import Card from './Card'
import cafe from './assets/cafe.png'
import chipa from './assets/chipa.png'
import cocidoConLeche from './assets/cocido con leche.png'
import cocidoNegro from './assets/cocido negro.png'
import mixto from './assets/mixto.png'

const Home = () => {
    const [productosList, setProductos] = useState([])

    useEffect(() => {
        fetch('http://localhost:8000/productos/', {
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setProductos(data)
            })
    }, [])

    const handleChangeName = (event) => {
        setName(event.target.value)
    }

    return (
        <>
            <h2>Productos para preparar la orden</h2>
            <ul>
                {productosList.map((producto) => {
                    return <li key={producto.id}>
                                <div>
                                    <h2>
                                    {producto.nombre}
                                    </h2>
                                    {producto.nombre === "cafe" ? (
                                        <img src={cafe} alt="Producto A" />
                                    ) : producto.nombre === "cocido con leche" ? (
                                            <img src={cocidoConLeche} alt="Producto C" />
                                    )  : producto.nombre === "mixto" ? (
                                        <img src={mixto} alt="Producto C" />
                                    )  : producto.nombre === "chipa" ? (
                                        <img src={chipa} alt="Producto C" />
                                    )   : (
                                        <img src={cocidoNegro} alt="Producto B" />
                                    )}
                                    <p>Precio: {producto.precio}</p>
                                    <button type="submit">Agregar</button>
                                </div>
                            </li>
                })}
            </ul>
        </>
    )
}

export default Home