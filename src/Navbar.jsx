import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = ({ user, logoutHandler }) => {
  return (
    <nav>
      <ul>
        <li><Link to="/">{user && <span>Bienvenido {user.username}</span>}</Link></li>
        <li><Link to="/pedidos">Pedidos</Link></li>
      </ul>
    </nav>
  );
}

export default Navbar;