import { useState, useEffect } from 'react'
import './App.css'
import Login from './Login'
import Home from './Home'
import jwtDecode from 'jwt-decode'
import { BrowserRouter } from 'react-router-dom';


function App() {
  const [userId, setUserId] = useState(null)

  useEffect(() => {
    const token = window.localStorage.getItem('accessToken')
    if (token){
      setUserId(jwtDecode(JSON.parse(token)).user_id)
    }
  }, [])

  const onLoginHandler = (userId) => {
    console.log(userId)
    setUserId(userId)
  }

  const onLogoutHandler = () => {
    setUserId(null)
    window.localStorage.removeItem('accessToken')
  }

  return (
    <BrowserRouter>
      {userId ? (
        <Home onLogout={onLogoutHandler} userId={userId} />
      ) : (
        <Login onLogin={onLoginHandler} />
      )}
    </BrowserRouter>
  )
}

export default App