import { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

const PedidoModal = ({ show, handleClose, handleSave, handleDelete, pedido, productos }) => {
  const [selectedProducto, setSelectedProducto] = useState('');
  const [cantidad, setCantidad] = useState(1);

  const handleProductoChange = (e) => {
    setSelectedProducto(e.target.value);
  };

  const handleCantidadChange = (e) => {
    setCantidad(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    handleSave(selectedProducto, cantidad);
    setSelectedProducto('');
    setCantidad(1);
  };

  const handleDeleteProducto = (producto) => {
    handleDelete(producto);
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Editar pedido</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="producto">
            <Form.Label>Producto</Form.Label>
            <Form.Control as="select" value={selectedProducto} onChange={handleProductoChange}>
              <option value="">Selecciona un producto</option>
              {productos.map((producto) => (
                <option key={producto.id} value={producto.id}>
                  {producto.nombre}
                </option>
              ))}
            </Form.Control>
          </Form.Group>
          <Form.Group controlId="cantidad">
            <Form.Label>Cantidad</Form.Label>
            <Form.Control type="number" value={cantidad} onChange={handleCantidadChange} />
          </Form.Group>
          <Button variant="primary" type="submit">
            Guardar
          </Button>
        </Form>
        <hr />
        <h5>Productos del pedido:</h5>
        {pedido.productos.map((producto) => (
          <p key={producto.id}>
            {producto.nombre} - {producto.cantidad} unidades{' '}
            <Button variant="danger" size="sm" onClick={() => handleDeleteProducto(producto)}>
              Eliminar
            </Button>
          </p>
        ))}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Cerrar
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default PedidoModal;